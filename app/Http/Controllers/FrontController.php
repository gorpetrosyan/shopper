<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        if(view()->exists('front.index')){
            return view('front.index');
        }
    }
}
