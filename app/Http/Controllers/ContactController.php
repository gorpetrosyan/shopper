<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __invoke()
    {
        if(view()->exists('front.contact_us')){
            return view('front.contact_us');
        }
    }
}
