<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Sub_Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(view()->exists('admin.sub_category')){
            $subs= Sub_Category::paginate(5);
//            dd($subs);
            return view ('admin.sub_category',['subs'=>$subs]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(view()->exists('admin.sub_category')){
            $categories = Category::all();
            return view('admin.sub_category_create',compact('categories'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sub_category' => 'required|max:255|min:2',
            'sub_desc' => 'max:255|min:2',
            'sub_key' => 'max:255|min:2',
            'category'=>'required|integer',
//            'common_id'=>'required|integer',
        ]);
        if($validator->fails()){
            return redirect()->route('sub_category.create')->withErrors($validator);
        }

        $sub = new Sub_Category();
        $sub->name= $request->sub_category;
        $sub->meta_d= $request->sub_desc;
        $sub->meta_k= $request->sub_key;
        $sub->category_id= $request->category;
        $sub->common_id= null;

        if($sub->save()){
            $sub->common_id = $sub->category_id;
            if($sub->update()){
                return redirect()->route('sub_category.index')->with('message','New Sub category was successfully created');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sub = Sub_Category::findOrFail($id);
        $categories = Category::All();
        return view('admin.sub_category_edit',compact('sub','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'sub_category' => 'required|max:255|min:2',
            'sub_desc' => 'max:255|min:2',
            'sub_key' => 'max:255|min:2',
            'category'=>'required|integer'
        ]);
        $sub = Sub_Category::findOrFail($id);
        $sub->name= $request->get('sub_category');
        $sub->meta_d= $request->get('sub_desc');
        $sub->meta_k= $request->get('sub_key');
        $sub->category_id= $request->get('category');
        $sub->save();

        return redirect('admin/sub_category')->with('message','Sub Category was successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub = Sub_Category::findOrFail($id);
        $sub->delete();
        if(view()->exists('admin.sub_category')){
//            $subs= Sub_Category::paginate(5);
//////            dd($subs);
////           $this->index();
            return redirect('admin/sub_category')->with('message','SubCategory was successfully deleted');
        }

//        return view('admin.brands_destroy')
    }
}
