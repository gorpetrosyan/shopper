<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Validator;

class CategoryController extends Controller
{
    public function show()
    {
        if(view()->exists('admin.category-list')){
            $category= Category::paginate(5);
            return view ('admin.category-list',['category'=>$category]);
        }
    }
    public function add()
    {
        if(view()->exists('admin.category-list')){
            return view('admin.category-add');
        }
    }
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:2'
        ]);
        if($validator->fails()){
            return redirect()->route('cat-add')->withErrors($validator);
        }

        $category = new Category();
        $category->title= $request->title;
        $category->save();
        return redirect()->route('cat-list')->with('success','New category was successfully created');
    }

    public function update($id)
    {
       $category = Category::findOrFail($id);
       return view('admin.category-edit',compact('category'));
    }

    public function category_create(Request $request,$id)
    {
//        dd($request['title']);
        $this->validate($request,[
            'title' => 'required|max:255|min:2'
        ]);
        $val = array(
            'title'=>$request['title']
          );
         $title = Category::findOrFail($id);
         $title->title=$request->get('title');
         $title->save();
//         dd($title['title']);


          return view('admin.category-handler',compact('title'))->with('message','Category was successfully updated');
    }
    public function delete($id)
    {
//        dd($id);
        $item = Category::findOrFail($id);
        $item->delete();
        return view('admin.category-delete')->with('message','Category was successfully deleted');
    }
}