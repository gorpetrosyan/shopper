<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\VerificationToken;
use Auth;
use App\Events\UserRequestedVerificationEmail;

class VerificationController extends Controller
{
    /**
     * @param VerificationToken $token
     * @return mixed
     */
    public function verify(VerificationToken $token)
    {
        // Получаем соответствующего пользователя и обговляем 'verified' на true
        // а потом удаляем token и перенаправляем пользователя на маршрут(  /login )
        $token->user()->update([
            'verified' => true
        ]);
        $token->delete();
        //Auth::login($token->user);
        return redirect('/')->withInfo('Проверка электронной почты прошла успешно. Пожалуйста, войдите снова.');
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function resend(Request $request)
    {
        $user = User::byEmail($request->email)->firstOrFail();
        if ($user->hasVerifiedEmail()) {
            return redirect('/home')->withInfo('Your email has been successfully checked.');
        }
        event(new UserRequestedVerificationEmail($user));
        return redirect('/login')->withInfo('Ups, something went wrong, Please check your email again!.');
    }
}