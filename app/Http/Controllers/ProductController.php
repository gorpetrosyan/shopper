<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function products()
    {
        if(view()->exists('front.products')){
            return view('front.products');
        }
    }

    public function single_product()
    {
        if(view()->exists('front.product_details')){
            return view('front.product_details');
        }
    }
}
