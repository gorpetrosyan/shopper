<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_Category extends Model
{
   public function Category()
   {
       return $this->belongsTo('App\Category');
   }
   public function Product()
   {
       return $this->hasMany('App\Product','sub__category_id','common_id');
   }
}
