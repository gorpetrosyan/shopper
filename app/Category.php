<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function  Sub_Category()
    {
        return $this->hasMany('App\Sub_Category','sub__category_id','common_id');
    }
    public function Product()
    {
        return $this->hasMany('App\Product');
    }
}
