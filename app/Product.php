<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
      'title',
      'availability',
        'condition',
        'quantity',
        'price',
        'web_id',
        'posters',
        'description',
        'meta_d',
        'meta_k',
    ];

    public function Category()
    {
      return $this->belongsTo('App\Category');
    }
    public function Sub_Category()
    {
      return $this->belongsTo('App\Sub_Category');
    }
}
