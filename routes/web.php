<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front routes
Route::group(['middleware' => 'web'], function (){
    Route::get('/','FrontController@index')->name('front');
    Route::get('/products','ProductController@products')->name('products');
    Route::get('/product/{id}','ProductController@single_product')->name('product');
    Route::get('/contact','ContactController')->name('contact');
//     Show payment form
    Route::get('/payment/add-funds/paypal', 'PaymentController@showForm');

// Post payment details for store/process API request
    Route::post('/payment/add-funds/paypal', 'PaymentController@store');

// Handle status
    Route::get('/payment/add-funds/paypal/status', 'PaymentController@getPaymentStatus');

// /Show payment form
    Route::auth();
});
// Front routes

//User dashboard
Route::group(['prefix' => 'cart', 'middleware' => 'auth'], function (){
    Route::get('/','CartController@index')->name('cart');
});
//User dashboard



// Admin panel
Route::group(['prefix'=>'admin', 'middleware' => ['auth', 'role']], function() {
    Route::get('/', function () {
        return view('home');
    })->name('admin-panel');
//category
    Route::get('/category-list',['uses'=>'Admin\CategoryController@show','as'=>'cat-list']);
    Route::get ('/category-add',['uses'=>'Admin\CategoryController@add','as'=>'cat-add']);
    Route::post('/category-create',['uses'=>'Admin\CategoryController@create','as'=>'cat-create']);
    Route::get('/category-update/{id}/edit',['uses'=>'Admin\CategoryController@update','as'=>'cat-update']);
    Route::patch('/category-handler/{id}',['uses'=>'Admin\CategoryController@category_create','as'=>'category-handler']);
    Route::delete('/category-delete/{id}',['uses'=>'Admin\CategoryController@delete','as'=>'category-delete']);
    Route::resource('/brands','Admin\BrandController');
    Route::resource('/sub_category','Admin\SubCategoryController');
    Route::resource('/products','Admin\ProductController');
});
// Admin panel

// Данный маршрут сработает при переходе из эл.почты в приложение, для подтверждения эл.почты
Route::get('/verify/token/{token}', 'Auth\VerificationController@verify')->name('auth.verify');
Route::get('/verify/resend', 'Auth\VerificationController@resend')->name('auth.verify.resend');

Auth::routes();









// Temporary route for inserting information in tables
//Route::get('/temp', 'TempController@insert');