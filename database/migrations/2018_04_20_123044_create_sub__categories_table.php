<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub__categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id')->unsigned();
//            $table->integer('common_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->string('meta_d');//meta description
            $table->string('meta_k');//meta key word
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('sub__categories');
        Schema::table('sub__categories', function (Blueprint $table) {
//            $table->index('category_id');
            $table->dropForeign('category_id');

        });
    }
}
