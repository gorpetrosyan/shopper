<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <title>PayPal</title>
    <style>
        div[class^="col"] {
            padding: 15px;
            border: 1px solid #eee;
            box-shadow: 0 0 20px #eee;
            margin-top: 200px;
        }
        h4 {
            background: #005EA6;
            text-align: center;
            text-transform: uppercase;
            font-size: 18px;
            color: #fff;
            padding: 10px 0;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <h4>Payment Form <span class="ion-card"></span></h4>
            <br>
            <form action="{{ url('/payment/add-funds/paypal') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="amount">Enter Amount</label>
                    <input type="text" name="amount" class="form-control" id="amount" aria-describedby="emailHelp" placeholder="Enter amount">
                </div>
                <button type="submit" class="btn btn-primary btn-sm btn-block">
                    Pay with PayPal
                    <span class="ion-paper-airplane"></span>
                </button>
            </form>
        </div>
    </div>
</div>







<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>