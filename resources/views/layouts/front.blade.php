<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#178EA5">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="register_route" content="{{route('register')}}">
    <meta name="login_route" content="{{route('login')}}">
    <title>@yield('title') | Core-Systems</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    @yield('css_plugins')
    <link rel="stylesheet" href="{{asset('css/index.css')}}">
    <link rel="stylesheet" href="{{asset('css/media.css')}}">

    <link rel="shortcut icon" type="image/png" href="{{asset('images/favicon.ico')}}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

@include('layouts.partials._notifications')

<!-- Button to top -->
@include('layouts.partials.button_to_top')
<!-- /Button to top -->

<!-- Header -->
@include('layouts.partials.header')
<!-- /Header -->

<!-- Carousel -->
@yield('carousel')
<!-- /Carousel -->

<!-- Main -->
<div class="container">
    <div class="row">
        <!-- Main Information -->
    @yield('content')
    <!-- /Main Information-->
    </div>
</div>
<!-- /Main -->

<!-- Footer -->
@include('layouts.partials.footer')
<!-- /Footer -->

<!-- Login and Register modal -->
@include('layouts.partials.login_register_modal')
<!-- /Login and Register modal -->

{{--<!-- Additonal modals -->--}}
@yield('modals')
{{--<!-- /Additonal modals-->--}}

<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
@yield('js_plugins')
<script src="{{asset('js/main.js')}}"></script>

<!-- Google map -->
@include('layouts.partials.google_map_script')
<!-- /Google map -->
</body>
</html>