<div class="row">
    <div class="col-md-6 col-md-offset-3">
        @if(session()->has('error'))
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {!! session()->get('error') !!}
            </div>
        @endif

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

    </div>
</div>