<div class="container">
    <div id="id_carousel1" class="carousel fade-slider" data-interval="6000">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#id_carousel1" data-slide-to="0" class="slider_indicators active"></li>
            <li data-target="#id_carousel1" data-slide-to="1" class="slider_indicators"></li>
            <li data-target="#id_carousel1" data-slide-to="2" class="slider_indicators"></li>
        </ol>

        <!-- Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-sm-offset-1 slide_information">
                            <h3>HP J9729A 2920-48</h3>
                            <h4>Layer 3 switch</h4>
                            <p>
                                48 x 10/100/1000 Gigabit Ethernet <br>
                                4 x SFP <br>
                                3 x Expansion Slot <br>
                                Web-interface
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-6 slide_picture">
                            <img class="img-responsive" src="images/hp_j9729a.jpg" alt="Image1">
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-sm-offset-1 slide_information">
                            <h3>Ubiquiti ES-48-Lite-EU</h3>
                            <h4>Layer 3 switch</h4>
                            <p>
                                48 x 10/100/1000 Gigabit Ethernet <br>
                                2x 10G SFP+ <br>
                                3 x Expansion Slot <br>
                                Up to 26 Gbps throughput
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-6 slide_picture">
                            <img class="img-responsive" src="images/ubiquiti_es-48-lite-eu.jpg" alt="Image2">
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-sm-offset-1 slide_information">
                            <h3>Super MicroAS-1022G</h3>
                            <h4>Server</h4>
                            <p>
                                2 x AMD Opteron 6320 (8x2.8GHz, L3 Cache 16MB) <br>
                                8Gb DDR3-1600 ECC <br>
                                4 x 3.5" SAS/SATA Hot-swap drives trays <br>
                                2 x Gigabit Ethernet
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-6 slide_picture">
                            <img class="img-responsive" src="images/supermicro_as-1022g.jpg" alt="Image3">
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Controls -->
        <a href="#id_carousel1" data-slide="prev">
            <i class="fa fa-angle-left left_arrow_carousel1"></i>
        </a>
        <a href="#id_carousel1" data-slide="next">
            <i class="fa fa-angle-right right_arrow_carousel1"></i>
        </a>
    </div>
</div>