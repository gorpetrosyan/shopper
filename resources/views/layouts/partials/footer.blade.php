<footer>
    <!-- Footer main -->
    <div class="container-fluid footer_main">
        <!-- Footer top -->
        <div class="container ">
            <div class="row">
                <!-- Footer top info -->
                <div class="col-xs-12 col-sm-2">
                    <div class="footer_top_info">
                        <h1>Core-Systems</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
                    </div>
                </div>
                <!-- /Footer top info -->
                <!-- Footer top videos -->
                <div class="col-xs-12 col-sm-6 ">
                    <div class="footer_top_icon_group">
                        <div class="col-xs-6 col-sm-3 footer_top_icon">
                            <a href="#">
                                <div>
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <img src="images/iframe1.jpg" alt="iframe1">
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <p>24 DEC 2014</p>
                        </div>
                        <div class="col-xs-6 col-sm-3 footer_top_icon">
                            <a href="#">
                                <div>
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <img src="images/iframe2.jpg" alt="iframe2">
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <p>24 DEC 2014</p>
                        </div>
                        <div class="col-xs-6 col-sm-3 footer_top_icon">
                            <a href="#">
                                <div>
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <img src="images/iframe3.jpg" alt="iframe3">
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <p>24 DEC 2014</p>
                        </div>
                        <div class="col-xs-6 col-sm-3 footer_top_icon">
                            <a href="#">
                                <div>
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <img src="images/iframe4.jpg" alt="iframe4">
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <p>24 DEC 2014</p>
                        </div>
                    </div>
                </div>
                <!-- /Footer top videos -->
                <!-- Map -->
                <div class="col-xs-12 col-sm-4">
                    <div id="map" class="map"></div>
                </div>
                <!-- /Map -->
            </div>
        </div>
        <!-- /Footer top -->
        <!-- Footer middle -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-4">
                        <div class="footer_navigation">
                            <h4>SERVICE</h4>
                            <ul class="nav nav-stacked">
                                <li><a href="#">Online Help</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Order Status</a></li>
                                <li><a href="#">Change Location</a></li>
                                <li><a href="#">FAQ’s</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="footer_navigation">
                            <h4>QUOCK SHOP</h4>
                            <ul class="nav nav-stacked">
                                <li><a href="#">T-Shirt</a></li>
                                <li><a href="#">Mens</a></li>
                                <li><a href="#">Womens</a></li>
                                <li><a href="#">Gift Cards</a></li>
                                <li><a href="#">Shoes</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="footer_navigation">
                            <h4>POLICIES</h4>
                            <ul class="nav nav-stacked">
                                <li><a href="#">Terms of Use</a></li>
                                <li><a href="#">Privecy Policy</a></li>
                                <li><a href="#">Refund Policy</a></li>
                                <li><a href="#">Billing System</a></li>
                                <li><a href="#">Ticket System</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-4 col-md-5">
                        <div class="footer_navigation">
                            <h4>ABOUT SHOP</h4>
                            <ul class="nav nav-stacked">
                                <li><a href="#">Company Information</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Store Location</a></li>
                                <li><a href="#">Affillate Program</a></li>
                                <li><a href="#">Copyright</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-7 col-md-7">
                        <div class="pull-left footer_navigation">
                            <h4>ABOUT SHOP</h4>
                            <form class="navbar-form">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your email address">
                                </div>
                                <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"
                                                                                 aria-hidden="true"></i></button>
                                <p>Get the most recent updates from our site and be updated your self...</p>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Footer middle -->
    </div>
    <!-- /Footer main -->
    <!-- Bottom line -->
    <div class="container-fluid bottom_line">
        <div>
            <p class="pull-left">Copyright © 2013 Core-Systems Inc. All rights reserved.</p>
            <p class="pull-right">Designed by <a href=""> Themeum</a></p>
        </div>
    </div>
    <!-- /Bottom line -->
</footer>