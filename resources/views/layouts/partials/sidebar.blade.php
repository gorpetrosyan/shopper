<div class="col-xs-12 col-sm-3 sidebar">
    <aside>
        <h3>CATEGORY</h3>
        <div class="panel-group panel_category" id="accordion">
            <div class="panel panel-default panel_main">
                <div class="panel-heading panel_heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Routers <i class="fa fa-plus pull-right" aria-hidden="true"></i>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body panel_brands">
                        <ul>
                            <li><a href="#">Core</a></li>
                            <li><a href="#">Edge</a></li>
                            <li><a href="#">Wired</a></li>
                            <li><a href="#">Wireless</a></li>
                            <li><a href="#">Virtual</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel_main">
                <div class="panel-heading panel_heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Switches <i class="fa fa-plus pull-right" aria-hidden="true"></i>
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body panel_brands">
                        <ul>
                            <li><a href="#">Layer 3</a></li>
                            <li><a href="#">Layer 2</a></li>
                            <li><a href="#">Unmanagable</a></li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="panel panel-default panel_main">
                <div class="panel-heading panel_heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                            Servers <i class="fa fa-plus pull-right" aria-hidden="true"></i>
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body panel_brands">
                        <ul>
                            <li><a href="#">Blade Servers</a></li>
                            <li><a href="#">Rack Servers</a></li>
                            <li><a href="#">Tower Servers</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel_links">
                <ul>
                    <li><a href="#">NAS</a></li>
                    <li><a href="#">Cooling Systems</a></li>
                    <li><a href="#">IP Phone</a></li>
                    <li><a href="#">UPS</a></li>
                    <li><a href="#">Software</a></li>
                </ul>
            </div>
        </div>
        <h3>BRANDS</h3>
        <div class="brands">
            <ul>
                <li><a href="#">CISCO <span class="badge pull-right">21</span></a></li>
                <li><a href="#">HP <span class="badge pull-right">43</span></a></li>
                <li><a href="#">SUPERMICRO <span class="badge pull-right">14</span></a></li>
                <li><a href="#">MIKROTIK <span class="badge pull-right">23</span></a></li>
                <li><a href="#">D-LINK <span class="badge pull-right">31</span></a></li>
                <li><a href="#">HUAWEI <span class="badge pull-right">32</span></a></li>
                <li><a href="#">UBIQUITI <span class="badge pull-right">26</span></a></li>
            </ul>
        </div>
        <h3>PRICE RANGE</h3>
        <div class="well text-center price_range">
            <input id="id_pricerange" type="text" class="span2" value="">
            <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
        </div>
        <div class="advertisement text-center">
            <img src="images/govazd.jpg" alt="advertisement">
        </div>
    </aside>
</div>