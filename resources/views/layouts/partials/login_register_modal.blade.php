<div id="id_modalloginregister" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content modal_login_register">
            <div class="modal-header modal_header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <ul id="id_login_register_menu" class="nav nav-tabs login_register_menu">
                    <li id="id_login_header" class="active login_register_tab_button"><a href="#id_login_tab">Login</a>
                    </li>
                    <li id="id_register_header" class="login_register_tab_button"><a
                                href="#id_register_tab">Register</a></li>
                </ul>
            </div>
            <div id="id_tab_content" class="tab-content modal-body">
                <div class="tab-pane fade in active login_tab" id="id_login_tab">
                    <form class="form-group" role="form" action=" {{ route('login') }}" method="POST">
                        {{ csrf_field() }}
                        <div id="success-m" class="hide">
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong>Welcome!</strong>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control input-lg" id="id_login_username"
                                   placeholder="User Email" name="email">
                            <span class="text-danger">
                                <strong id="loginEmail-error"></strong>
                            </span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control input-lg" id="id_login_password"
                                   placeholder="Password" name="password">
                            <span class="text-danger">
                                <strong id="loginPassword-error"></strong>
                            </span>
                        </div>
                        <input id="id_signed_in" type="checkbox">
                        <label for="id_signed_in">Keep me signed in</label>
                        <button type="submit" id="submitIn" class="btn  center-block login_btn">Sign in</button>
                    </form>
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                </div>
                <div class="tab-pane fade register_tab" id="id_register_tab">
                    <div id="success-msg" class="hide">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>Success!</strong> Check your mail for login confirmation!!
                        </div>
                    </div>
                    <form id="register" class="form-group" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class=" form-group has-feedback">
                            <input type="text"  class="form-control input-lg" id="id_register_username"
                                   placeholder="Name" name="name">
                            {{--<span class="glyphicon glyphicon-user form-control-feedback"></span>--}}
                            <span class="text-danger">
                                <strong id="name-error"></strong>
                            </span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text"  class="form-control input-lg" id="id_register_surname"
                                   placeholder="Surname" name="surname">
                            {{--<span class="glyphicon glyphicon-user form-control-feedback"></span>--}}
                            <span class="text-danger">
                                <strong id="surname-error"></strong>
                            </span>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text"  class="form-control input-lg" id="id_register_email"
                                   placeholder="Email Address" name="email">
                            {{--<span class="glyphicon glyphicon-user form-control-feedback"></span>--}}
                            <span class="text-danger">
                                <strong id="email-error"></strong>
                            </span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control input-lg" id="id_register_password"
                                   placeholder="Password" name="password">
                            {{--<span class="glyphicon glyphicon-user form-control-feedback"></span>--}}
                            <span class="text-danger">
                                <strong id="password-error"></strong>
                            </span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control input-lg" id="id_register_confirm_password"
                                   placeholder="Confirm password" name="password_confirmation" >
                            {{--<span class="glyphicon glyphicon-log-in form-control-feedback"></span>--}}

                        </div>
                        <div class="row">
                        <div class="col-xs-12 text-center">
                        <button type="submit" id="submitUp" class="btn center-block register_btn">Sign up</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>