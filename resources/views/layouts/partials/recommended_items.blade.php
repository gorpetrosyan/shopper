<h3>RECOMMENDED ITEMS</h3>
<div id="id_carousel2" class="carousel slide">
    <!-- Slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="text-center carousel_only_product">
                        <div class="carousel_product_image ">
                            <a href="#"><img src="images/d-Link_dir-632.jpg" alt="product1"></a>
                        </div>
                        <p>$56</p>
                        <a href="#">D-Link DIR-632</a>
                        <button type="button" class="btn btn-default">View</button>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="text-center carousel_only_product">
                        <div class="carousel_product_image">
                            <a href="#"><img src="images/d-link_dir-890lr.jpg" alt="product1"></a>
                        </div>
                        <p>$56</p>
                        <a href="#">D-Link DIR-890LR</a>
                        <button type="button" class="btn btn-default">View</button>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="text-center carousel_only_product">
                        <div class="carousel_product_image">
                            <a href="#"><img src="images/mikrotik_hap_lite.jpg"
                                             alt="product1"></a>
                        </div>
                        <p>$56</p>
                        <a href="#">Mikrotik hAP lite</a>
                        <button type="button" class="btn btn-default">View</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="text-center carousel_only_product">
                        <div class="carousel_product_image">
                            <a href="#"><img src="images/d-Link_dir-632.jpg" alt="product1"></a>
                        </div>
                        <p>$56</p>
                        <a href="#">D-Link DIR-632</a>
                        <button type="button" class="btn btn-default">View</button>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="text-center carousel_only_product">
                        <div class="carousel_product_image">
                            <a href="#"><img src="images/d-link_dir-890lr.jpg" alt="product1"></a>
                        </div>
                        <p>$56</p>
                        <a href="#">D-Link DIR-890LR</a>
                        <button type="button" class="btn btn-default">View</button>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="text-center carousel_only_product">
                        <div class="carousel_product_image">
                            <a href="#"><img src="images/mikrotik_hap_lite.jpg"
                                             alt="product1"></a>
                        </div>
                        <p>$56</p>
                        <a href="#">Mikrotik hAP lite</a>
                        <button type="button" class="btn btn-default">View</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="text-center carousel_only_product">
                        <div class="carousel_product_image">
                            <a href="#"><img src="images/d-link_des-1100-16.jpg" alt="product1"></a>
                        </div>
                        <p>$56</p>
                        <a href="#">D-Link DIR-632</a>
                        <button type="button" class="btn btn-default">View</button>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="text-center carousel_only_product">
                        <div class="carousel_product_image">
                            <a href="#"><img src="images/d-link_dir-890lr.jpg" alt="product1"></a>
                        </div>
                        <p>$56</p>
                        <a href="#">D-Link DIR-890LR</a>
                        <button type="button" class="btn btn-default">View</button>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="text-center carousel_only_product">
                        <div class="carousel_product_image">
                            <a href="#"><img src="images/mikrotik_hap_lite.jpg"
                                             alt="product1"></a>
                        </div>
                        <p>$56</p>
                        <a href="#">Mikrotik hAP lite</a>
                        <button type="button" class="btn btn-default">View</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="" href="#id_carousel2" data-slide="prev">
        <i class="fa fa-angle-left left_arrow_carousel2"></i>
    </a>
    <a class="" href="#id_carousel2" data-slide="next">
        <i class="fa fa-angle-right right_arrow_carousel2"></i>
    </a>
</div>