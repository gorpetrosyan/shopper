@extends('layouts.front')

@section('title','Product Details')

@section('css_plugins')
    <link rel="stylesheet" href="{{asset('css/bootstrap-slider.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-touch-slider.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
@endsection

@section('content')
    @include('layouts.partials.sidebar')

    <div class="col-xs-12 col-sm-9 main_information">

        <div class="row">
            <div class="col-xs-12 col-sm-5">
                <div id="id_viewcurrentproduct" class="view_product middle">
                    <img src="images/d-link_dir-890lr.jpg" alt="product_view_1">
                </div>
                <!-- Controls -->
                <div id="id_owl_nav_controls" class="owl-nav"></div>
                <!-- Owl carousel-->
                <div class="owl-carousel owl-theme product_images_carousel" id="id_owl_carousel_product_images">
                    <div class="item"><img src="images/d-link_dir-890lr.jpg" alt="product_current_image"></div>
                    <div class="item"><img src="images/d-link_dir-890lr_1.jpg" alt="product_small_image1_1"></div>
                    <div class="item"><img src="images/d-link_dir-890lr_2.jpg" alt="product_small_image1_2"></div>
                    <div class="item"><img src="images/d-link_dir-890lr_3.jpg" alt="product_small_image1_3"></div>
                    <div class="item"><img src="images/d-link_dir-890lr_4.jpg" alt="product_small_image1_4"></div>
                    <div class="item"><img src="images/d-link_dir-890lr_5.jpg" alt="product_small_image1_5"></div>
                    <div class="item"><img src="images/d-link_dir-890lr_6.jpg" alt="product_small_image1_6"></div>
                    <div class="item"><img src="images/d-link_dir-890lr_7.jpg" alt="product_small_image1_7"></div>
                </div>
                <!-- /Owl carousel-->
            </div>
            <div class="col-xs-12 col-sm-7">
                <div class="product_information">
                    <h5 id="id_productname">D-Link DIR-890L</h5>
                    <p class="product_web_id">Web ID: 1089772</p>
                    <img src="images/rating.png" alt="Rating">
                    <p class="product_price">US $139</p>
                    <form class="">
                        <label class="" for="id_product_details_quantity"> Quantity: </label>
                        <input type="text" class="form-control" id="id_product_details_quantity" value="1"
                               autocomplete="off">
                        <button type="button" class="btn btn-default btn_add_to_cart">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> Add to cart
                        </button>
                    </form>
                    <p><b>Availability:</b> In Stock</p>
                    <p><b>Condition:</b> New</p>
                    <p><b>Brand:</b> D-Link</p>
                    <a href="#"><img src="images/icon_facebook.png" alt="facebook"></a>
                    <a href="#"><img src="images/icon_tweet.png" alt="tweet"></a>
                    <a href="#"><img src="images/icon_pin_it.png" alt="pin it"></a>
                    <a href="#"><img src="images/icon_share.png" alt="share"></a>
                </div>
            </div>
        </div>

        @include('layouts.partials.recommended_items')
    </div>

@endsection

@section('modals')
    @include('layouts.partials.view_picture_modal')
@endsection

@section('js_plugins')
    <script src="{{asset('js/bootstrap-slider.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-touch-slider-min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/jquery.ez-plus.js')}}"></script>
    <script src="{{asset('js/swipe.js')}}"></script>
@endsection