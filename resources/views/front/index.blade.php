@extends('layouts.front')

@section('title','Home')

@section('css_plugins')
    <link rel="stylesheet" href="{{asset('css/bootstrap-slider.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-touch-slider.css')}}" media="all">
@endsection

@section('carousel')
    @include('layouts.partials.carousel')
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
    @include('layouts.partials.sidebar')

    <!-- Main Information -->
    <div class="col-xs-12 col-sm-9 main_information">
        <!-- Features items -->
        <h3>FEATURES ITEMS</h3>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="product">
                    <div class="text-center only_product">
                        <div class="only_product_image">
                            <a href="#"><img src="images/cisco_sg300-28pp.jpg" alt="product1"></a>
                        </div>
                        <p>$295</p>
                        <a href="product_details.html">Cisco SG300-28PP</a>
                    </div>
                    <div class="select">
                        <ul class="nav nav-pills nav-justified">
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="product">
                    <div class="text-center only_product">
                        <div class="only_product_image">
                            <a href="#"><img src="images/linksys_spa8000.jpg" alt="product1"></a>
                        </div>
                        <p>$119</p>
                        <a href="product_details.html">Linksys SPA8000</a>
                    </div>
                    <div class="select">
                        <ul class="nav nav-pills nav-justified">
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="product">
                    <div class="text-center only_product">
                        <div class="only_product_image">
                            <a href="#"><img src="images/cisco_sf110-16.jpg" alt="product1"></a>
                        </div>
                        <p>$56</p>
                        <a href="product_details.html">Cisco SF110-16</a>
                    </div>
                    <div class="select">
                        <ul class="nav nav-pills nav-justified">
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="product">
                    <div class="text-center only_product">
                        <div class="only_product_image">
                            <a href="#"><img src="images/mikroTik_3011uias%20rm.jpg" alt="product1"></a>
                        </div>
                        <p>$140</p>
                        <a href="product_details.html">MikroTik 3011UiAS RM</a>
                    </div>
                    <div class="select">
                        <ul class="nav nav-pills nav-justified">
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="product">
                    <div class="text-center only_product">
                        <div class="only_product_image">
                            <a href="#"><img src="images/d-link_des-1100-16.jpg"
                                             alt="product1"></a>
                        </div>
                        <p>$142</p>
                        <a href="product_details.html">D-Link DES-1100-16</a>
                    </div>
                    <div class="select">
                        <ul class="nav nav-pills nav-justified">
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="product">
                    <div class="text-center only_product">
                        <div class="only_product_image">
                            <a href="#"><img src="images/d-Link_dir-632.jpg" alt="product1"></a>
                        </div>
                        <p>$73</p>
                        <a href="product_details.html">D-Link DIR-632</a>
                    </div>
                    <div class="select">
                        <ul class="nav nav-pills nav-justified">
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>
                            <li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Features items -->
        <!-- Recommended items -->
    @include('layouts.partials.recommended_items')
    <!-- /Recommended items -->
    </div>
    <!-- /Main Information-->
@endsection

@section('js_plugins')
    <script src="{{asset('js/bootstrap-slider.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-touch-slider-min.js')}}"></script>
@endsection