@extends('layouts.front')

@section('title','Cart')

@section('content')
    <hr>
        <div class="table-responsive cart_table_div">
            <table class="table ">
                <thead>
                <tr class="cart_head">
                    <th>Item</th>
                    <th></th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="cart_image">
                        <a href="#"><img src="images/d-Link_dir-632.jpg" alt="Product 1"></a>
                    </td>
                    <td class="cart_description">
                        <a href="#">D-Link DIR-632</a>
                        <p>Web ID: 1089772</p>
                    </td>
                    <td class="cart_price">
                        <p class="cart_unit_price">$73</p>
                    </td>
                    <td class="cart_quantity">
                        <button type="button" class="cart_quantity_minus">-</button>
                        <input class="form-control cart_quantity_input" type="text" value="1" autocomplete="off">
                        <button type="button" class="cart_quantity_plus">+</button>
                    </td>
                    <td class="cart_total">
                        <p class="cart_total_price"></p>
                        <button class="btn btn-default">BUY</button>
                    </td>
                    <td class="cart_remove">
                        <button class="btn btn-default cart_remove_button" type="button">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="cart_image">
                            <a href="#"><img src="images/hp_g1610t.jpg" alt="Product 2"></a>
                        </div>
                    </td>
                    <td class="cart_description">
                        <a href="#">HP G1610T MicroServer</a>
                        <p>Web ID: 1089684</p>
                    </td>
                    <td class="cart_price">
                        <p class="cart_unit_price">$1864</p>
                    </td>
                    <td class="cart_quantity">
                        <button type="button" class="cart_quantity_minus">-</button>
                        <input class="form-control cart_quantity_input" type="text" value="1" autocomplete="off">
                        <button type="button" class="cart_quantity_plus">+</button>
                    </td>
                    <td class="cart_total">
                        <p class="cart_total_price"></p>
                        <button class="btn btn-default">BUY</button>
                    </td>
                    <td class="cart_remove">
                        <button class="btn btn-default cart_remove_button" type="button">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="cart_image">
                            <a href="#"><img src="images/synology_ds118.jpg" alt="Product 3"></a>
                        </div>
                    </td>
                    <td class="cart_description">
                        <a href="#">Synology DS118</a>
                        <p>Web ID: 1089491</p>
                    </td>
                    <td class="cart_price">
                        <p class="cart_unit_price">$205</p>
                    </td>
                    <td class="cart_quantity">
                        <button type="button" class="cart_quantity_minus">-</button>
                        <input class="form-control cart_quantity_input" type="text" value="1" autocomplete="off">
                        <button type="button" class="cart_quantity_plus">+</button>
                    </td>
                    <td class="cart_total">
                        <p class="cart_total_price"></p>
                        <button class="btn btn-default">BUY</button>
                    </td>
                    <td class="cart_remove">
                        <button class="btn btn-default cart_remove_button" type="button">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="cart_image">
                            <a href="#"><img src="images/mikrotik_hap_lite.jpg" alt="Product 4"></a>
                        </div>
                    </td>
                    <td class="cart_description">
                        <a href="#">Mikrotik hAP Lite</a>
                        <p>Web ID: 1089732</p>
                    </td>
                    <td class="cart_price">
                        <p class="cart_unit_price">$22</p>
                    </td>
                    <td class="cart_quantity">
                        <button type="button" class="cart_quantity_minus">-</button>
                        <input class="form-control cart_quantity_input" type="text" value="1" autocomplete="off">
                        <button type="button" class="cart_quantity_plus">+</button>
                    </td>
                    <td class="cart_total">
                        <p class="cart_total_price"></p>
                        <button class="btn btn-default">BUY</button>
                    </td>
                    <td class="cart_remove">
                        <button class="btn btn-default cart_remove_button" type="button">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
                <tr class="total_price">
                    <td>
                        <p>Total Price</p>
                    </td>
                    <td class="cart_description">

                    </td>
                    <td class="cart_price">

                    </td>
                    <td class="cart_quantity">

                    </td>
                    <td class="cart_total">
                        <p class="cart_total_price" id="id_total_amount"></p>
                        <button class="btn btn-default">BUY ALL</button>
                    </td>
                    <td class="cart_remove">

                    </td>
                </tr>
                </tbody>
            </table>
        </div>
@endsection