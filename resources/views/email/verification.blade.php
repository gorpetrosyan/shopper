Please verify your email

<a style="
display: inline-block;
padding: 7px 20px;
background-color: dodgerblue;
color: #fff;
outline: none;
text-decoration: none;
border-radius: 5px;"
   href="{{route('auth.verify', $token)}}">
    Verify your email
</a>