@extends('layouts.app')
@section('content')

    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <br />
        <div class="col-lg-1">
            <a href="{{ url('admin/category-list') }}" class="btn btn-success" >Go to Categories</a>
        </div>

    </div>

        @endsection