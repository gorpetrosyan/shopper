@extends('layouts.app')

@section('content')

    <div class="col-xs-12">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <br />
        <h1>{{$sub->name}} Sub_Categories was successfully updated</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{ url('admin/sub_category') }}" class="btn btn-success" >Return Back</a>
        <br><br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>description</th>
                <th>Keys</th>
                <th>Belongs to</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
                    <tr>
                        <td>{{$sub['id']}}</td>
                        <td>{{$sub->name}}</td>
                        <td>{{$sub->meta_d}}</td>
                        <td>{{$sub->meta_k}}</td>
                        <td>{{$sub->category->title}}</td>
                        <td>{{$sub->created_at}}</td>
                        <td>{{$sub->updated_at}}</td>
                        <td>
                            <a href="{{route('sub_category.edit',['id'=>$sub->id])}}" data-toggle="tooltip"  title="Update Sub Category" class="btn btn-info btn-sm"  role="button">Edit</a>

                        </td>
                        <td>
                            {{--delete--}}
                            <form action="{{route('sub_category.destroy',$sub['id'])}}" method="post">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                                    Delete
                                </button>

                                {{--<!-- Modal -->--}}
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Are you Sure? </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                 You are going to delete <span  style="color: red; text-transform: uppercase">{{$sub->name}}</span>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
@endsection