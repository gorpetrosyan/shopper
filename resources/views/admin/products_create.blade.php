@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <h1>Add Products</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data">
            {{ @csrf_field() }}
            <div class="form-group">
                <label for="brand-add">Title</label>
                <input name="title" type="text" class="form-control" placeholder="Add Title...">
                {{--available--}}
                <hr>
                <h5>Availability</h5>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="availability" id="inlineRadio1" value="yes">
                    <label class="form-check-label" for="inlineRadio1">Available</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="availability" id="inlineRadio2" value="no">
                    <label class="form-check-label" for="inlineRadio2">Don`t Available</label>
                </div>
                {{--end available--}}
                <hr>
                {{--condition--}}
                <label for="condition">Condition</label>
                <input id="condition" name="condition" type="text" class="form-control" placeholder="Add Condition...">
                {{--end condition--}}
                {{--quantity--}}
                <hr>
                <h4>Quantity</h4>
                <select name="quantity" class="custom-select" style="width: 10%;height: 32px">
                @for($i = 0;$i<=1000;$i++)
                    <option value="{{$i}}">{{$i}}</option>
                  @endfor
                </select>
                {{--end quantity--}}
                <hr>
                {{--price--}}
                <label for="price">Price</label>
                <input id="price" name="price" type="text" class="form-control" placeholder="Add Price...">
                {{--end price--}}
                <hr>
                {{--web_id--}}
                <h5>Click on button to generate web_id</h5>
                <button type="button" id="web" name="web_id" onclick=" {
                       $('#web').val((new Date().getSeconds()).toString() + (Math.floor(Math.random()*999)+10).toString()+(new Date().getMinutes()).toString()+(new Date().getHours()).toString()+(new Date().getDate()).toString()) ;
                       $('#web').html($('#web').val())
                        }" class="btn btn-info">Click Me</button>
                {{--End web_id--}}
                <hr>
                {{--poster--}}
                    {{--<input type="file" id="poster"  class="custom-file-input" name="poster[]" multiple/>--}}
                <input type="file" class="filestyle" id="poster"  name="poster[]" multiple/>
                {{--End poster--}}
                <hr>
                {{--description--}}
                <h4 >Description</h4>
                <textarea class="form-control" title="description" name="description" id="description" rows="10" cols="80">
            </textarea>
                {{--end description--}}
                <hr>
            </div>
            <button type="submit" class="btn btn-success">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Add Product
            </button>
        </form>

        <br />
        <br>
        <br>
        <div class="col-lg-1 ">
            <a href="{{ url('admin/products') }}" class="btn btn-success" style="margin-left: 200%;margin-top: -190px" >Go to Products</a>
        </div>
    </div>

@endsection