@extends('layouts.app')
@section('content')

<div class="container">
    <h1 class="text-center">Edit {{$category['title']}}</h1>

    <div class="row">
        {{--form group--}}
        <form action="{{route('category-handler',$category['id'])}}" method="post">
            {{ csrf_field() }}
            <div class="col-lg-3">
                <input name="_method" type="hidden" value="PATCH">
                <div class="input-group">
                    <input type="text" class="form-control" name="title" value="{{$category['title']}}" placeholder="Title" style="width: 120%">
                </div>
            </div>
            <div class="col-lg-1">
                <button type="submit" class="btn btn-danger">Update</button>
            </div>
        </form>
        <div class="col-lg-1">
            <a href="{{ url('admin/category-list') }}" class="btn btn-success" >Go to Categories</a>
        </div>
    </div>



</div>




    @endsection