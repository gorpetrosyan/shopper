@extends('layouts.app')
@section('content')


    <div class="container">
        <h1 class="text-center">Your new update is {{$title['title']}}</h1>

        <div class="col-xs-12">
            <table class="table table-bordered table-hover table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                        <tr>
{{--                            @foreach($title as $cat)--}}
                            <td>{{$title['id']}}</td>
                            <td>{{$title['title']}}</td>
                            <td>{{$title['created_at']}}</td>
                            <td>{{$title['updated_at']}}</td>
                            <td>
                                <a href="{{route('cat-update',['id'=>$title->id])}}" data-toggle="tooltip"  title="Update category" class="btn btn-info btn-sm"  role="button">Edit</a>

                            </td>
                            <td>
                                <form action="{{route('category-delete',$title['id'])}}" method="post">
                                    {{csrf_field()}}
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-danger"  type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                </tbody>
            </table>
        </div>

        <div class="row">
            {{--form group--}}

            <div class="col-lg-1">
                <a href="{{ url('admin/category-list') }}" class="btn btn-success" >Go to Categories</a>
            </div>
        </div>



    </div>



    @endsection