@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <h1>Add Brand</h1>
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('brands.store') }}" method="post">
            {{ @csrf_field() }}
            <div class="form-group">
                <label for="brand-add">Add new Brand</label>
                <input name="brand" type="text" class="form-control" id="brand-add" placeholder="Add new Brand...">
            </div>
            <button type="submit" class="btn btn-success">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Add Brand
            </button>
        </form>

        <br />
        <br>
        <br>
        <div class="col-lg-1 ">
            <a href="{{ url('admin/brands') }}" class="btn btn-success" style="margin-left: 200%;margin-top: -190px" >Go to Brands</a>
        </div>
    </div>

@endsection