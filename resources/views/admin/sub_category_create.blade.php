@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <h1>Add Sub_Category</h1>
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('sub_category.store') }}" method="post">
            {{ @csrf_field() }}
            <div class="form-group">
                <label for="brand-add">Add new Sub_Category</label>
                <input name="sub_category" type="text" class="form-control" id="sub-add" placeholder="Add new Sub Category...">
                <input type="text" name="sub_desc" placeholder="Describe your sub category" class="form-control" id="sub-desc">
                <input type="text" name="sub_key" placeholder="Meta keys for your sub category" class="form-control" id="sub-key">
                <select class="form-control mt-5" name="category" title="category">

                    @if($categories->count())

                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                        @endforeach
                    @endif

                </select>
            </div>
            <button type="submit" class="btn btn-success">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Add Sub_Category
            </button>
        </form>

        <br />
        <br>
        <br>
        <div class="col-lg-1 mb-20">
            <a href="{{ url('admin/sub_category') }}" class="btn btn-success" style="margin-left: 200%;margin-top: -150px" >Go to Sub_Categories</a>
        </div>
    </div>

@endsection