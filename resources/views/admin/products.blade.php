@extends('layouts.app')
@section('content')


    <div class="col-xs-12">
        <br/>
        <h1>All Products</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('products.create')}}" class="btn btn-success">
            Add Products
        </a>
        <br><br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="myTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Brand</th>
                <th>Condition</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Order_id</th>
                <th>Images</th>
                <th>Description</th>
                <th>Meta_Desc</th>
                <th>Meta_Keys</th>
                <th>Belongs to Sub</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($prods) && is_object($prods))
                @foreach($prods as $prod)
                    <tr>
                        <td>{{$prod['id']}}</td>
                        <td id="{{$prod->id}}">{{$prod->title}}</td>
                        <td>{{$prod->brands->Brand}}</td>
                        <td>{{$prod->condition}}</td>
                        {{--$prod->condition its mean weither its new or old or normal product--}}
                        <td>{{$prod->quantity}}</td>
                        <td>{{$prod->price}}</td>
                        <td>{{$prod->web_id}}</td>
                        <td>{{$prod->posters}}</td>
                        <td>{{$prod->description}}</td>
                        <td>{{$prod->meta_d}}</td>
                        <td>{{$prod->meta_k}}</td>
                        <td>{{$prod->sub__categories->common}}</td>
                        <td>{{$prod->created_at}}</td>
                        <td>{{$prod->updated_at}}</td>
                        <td>
                            {{--<a href="{{route('sub_category.edit',['id'=>$sub->id])}}" data-toggle="tooltip"--}}
                               {{--title="Update Sub Category" class="btn btn-info btn-sm" role="button">Edit</a>--}}

                        {{--</td>--}}
                        {{--<td>--}}
                            {{--{{$a= }}'<span style="color: red">{{$sub->name}}</span>'--}}
                            {{--<form action="{{route('sub_category.destroy',$sub['id'])}}" method="post"--}}
                                  {{--onsubmit="return confirm('You are going to delete  {{$sub->name}}')">--}}
                                {{--{{csrf_field()}}--}}
                                {{--{{method_field('DELETE')}}--}}
                                {{--<button type="submit" class="btn btn-danger">Delete</button>--}}
                            {{--</form>--}}
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {{$prods->links()}}
    </div>



    @endsection