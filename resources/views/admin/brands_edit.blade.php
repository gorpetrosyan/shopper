@extends('layouts.app')
@section('content')

    <div class="container">
        <h1 class="text-center">Edit {{$brand['Brand']}}</h1>
<br>
        <hr>
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            {{--form group--}}
            <form action="{{route('brands.update',$brand['id'])}}" method="post">
                {{ csrf_field() }}
                <div class="col-lg-3">
                    {{--<input name="_method" type="hidden" value="PATCH">--}}
                    {{method_field('PUT')}}
                    <div class="input-group">
                        <input type="text" class="form-control" name="brand" value="{{$brand['Brand']}}" placeholder="Title" style="width: 120%">
                    </div>
                </div>
                <div class="col-lg-1">
                    <button type="submit" class="btn btn-danger">Update</button>
                </div>
            </form>
            <div class="col-lg-1">
                <a href="{{ url('admin/brands') }}" class="btn btn-success" >Go to Brands</a>
            </div>
        </div>



    </div>
    @endsection