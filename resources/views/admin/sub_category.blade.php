@extends('layouts.app')

@section('content')

    <div class="col-xs-12">
        <br/>
        <h1>All Sub_Categories</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('sub_category.create')}}" class="btn btn-success">
            Add Sub_Categories
        </a>
        <br><br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="myTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>description</th>
                <th>Keys</th>
                <th>Belongs to</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($subs) && is_object($subs))
                @foreach($subs as $sub)
                    <tr>
                        <td>{{$sub['id']}}</td>
                        <td id="{{$sub->id}}">{{$sub->name}}</td>
                        <td>{{$sub->meta_d}}</td>
                        <td>{{$sub->meta_k}}</td>
                        <td>{{$sub->category->title}}</td>
                        <td>{{$sub->created_at}}</td>
                        <td>{{$sub->updated_at}}</td>
                        <td>
                            <a href="{{route('sub_category.edit',['id'=>$sub->id])}}" data-toggle="tooltip"
                               title="Update Sub Category" class="btn btn-info btn-sm" role="button">Edit</a>

                        </td>
                        <td>
                            {{--{{$a= }}'<span style="color: red">{{$sub->name}}</span>'--}}
                            <form action="{{route('sub_category.destroy',$sub['id'])}}" method="post"
                                  onsubmit="return confirm('You are going to delete  {{$sub->name}}')">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {{$subs->links()}}
    </div>

@endsection
