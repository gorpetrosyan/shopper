@extends('layouts.app')

@section('content')

    <div class="col-xs-12">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <br />
        <h1>All Category</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('cat-add')}}" class="btn btn-success">
            Add category
        </a>
        <br><br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($category) && is_object($category))
                @foreach($category as $cat)
                    <tr>
                        <td>{{$cat['id']}}</td>
                        <td id="{{$cat['id']}}">{{$cat->title}}</td>
                        <td>{{$cat->created_at}}</td>
                        <td>{{$cat->updated_at}}</td>
                        <td>
                            <a href="{{route('cat-update',['id'=>$cat->id])}}" data-toggle="tooltip"  title="Update category" class="btn btn-info btn-sm"  role="button">Edit</a>

                        </td>
                        <td>
                            {{--delete--}}
                            <form action="{{route('category-delete',$cat['id'])}}" method="post">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button type="button" onclick=" {
                                {{--                                        var a = {{$sub['id']}}--}}
                                        document.getElementById('sp').innerHTML = document.getElementById('{{$cat['id']}}').innerHTML;

                                        }" class="btn btn-danger"  data-toggle="modal" data-target="#exampleModal">
                                    Delete
                                </button>

                                {{--<!-- Modal -->--}}
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Are you Sure? </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                You are going to delete <span id="sp"  style="color:red;text-transform: uppercase"></span>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {{$category->links()}}
    </div>
@endsection
