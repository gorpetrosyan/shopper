@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <h1>Add category</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('cat-create') }}" method="post">
            {{ @csrf_field() }}
            <div class="form-group">
                <label for="category-add">Add new category</label>
                <input name="title" type="text" class="form-control" id="category-add" placeholder="Add new category...">
            </div>
            <button type="submit" class="btn btn-success">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Add category
            </button>
        </form>
    </div>
@endsection