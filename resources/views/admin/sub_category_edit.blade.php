@extends('layouts.app')
@section('content')

    <div class="container">
        <h1 class="text-center">Edit {{$sub['name']}}</h1>
        <br>
        <hr>
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            {{--form group--}}
            <form action="{{route('sub_category.update',$sub['id'])}}" method="post">
                {{ csrf_field() }}
                <div class="col-lg-3">
                    {{--<input name="_method" type="hidden" value="PATCH">--}}
                    {{method_field('PUT')}}
                    <div class="input-group">
                        <input type="text" class="form-control" name="sub_category" value="{{$sub['name']}}" placeholder="sub name">
                        <input type="text" class="form-control" name="sub_desc" value="{{$sub['meta_d']}}" placeholder="sub description">
                        <input type="text" class="form-control" name="sub_key" value="{{$sub['meta_k']}}" placeholder="sub meta key">
                        <select class="form-control mt-5" name="category" title="category">

                            @if($categories->count())

                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
                <div class="col-lg-1">
                    <button type="submit" class="btn btn-danger">Update</button>
                </div>
            </form>
            <div class="col-lg-1">
                <a href="{{ url('admin/sub_category') }}" class="btn btn-success" >Return Back</a>
            </div>
        </div>



    </div>
@endsection