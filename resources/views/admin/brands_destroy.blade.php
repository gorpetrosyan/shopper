@extends('layouts.app')
@section('content')

    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <br />

            <h1> The Data was successfully deleted</h1>
        <div class="col-lg-1">
            <a href="{{ url('admin/brands') }}" class="btn btn-success" >Go to Brands</a>
        </div>

    </div>

@endsection