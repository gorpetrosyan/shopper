@extends('layouts.app')

@section('content')

    <div class="col-xs-12">
        {{--@if(session()->has('message'))--}}
            {{--<div class="alert alert-success">--}}
                {{--{{ session()->get('message') }}--}}
            {{--</div>--}}
        {{--@endif--}}
        <br />
        <h1>All Brands</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('brands.create')}}" class="btn btn-success">
            Add Brands
        </a>
        <br><br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($brands) && is_object($brands))
                @foreach($brands as $brand)
                    <tr>
                        <td>{{$brand['id']}}</td>
                        <td id="{{$brand['id']}}">{{$brand->Brand}}</td>
                        <td>{{$brand->created_at}}</td>
                        <td>{{$brand->updated_at}}</td>
                        <td>
                            <a href="{{route('brands.edit',['id'=>$brand->id])}}" data-toggle="tooltip"  title="Update Brand" class="btn btn-info btn-sm"  role="button">Edit</a>

                        </td>
                        <td>
                            {{--delete--}}
                            <form action="{{route('brands.destroy',$brand['id'])}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <!-- Button trigger modal -->
                                <button type="button" onclick=" {
                                {{--                                        var a = {{$sub['id']}}--}}
                                        document.getElementById('sp').innerHTML = document.getElementById('{{$brand['id']}}').innerHTML;

                                        }" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                                    Delete
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Are you Sure? </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Be careful,cause of you can't reBack DATA
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {{$brands->links()}}
    </div>
@endsection
